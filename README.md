AudioHTML
=================

Una demostración de como usar audio desde HTML

Your Project
------------

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← accion.html

Ejemplo de como hace que la musica suene al presionar un boton aparte de los controles.

### ← index.html

La pagina principal

### ← style.css

CSS files add styling rules to your content.

### ← script.js

If you're feeling fancy you can add interactivity to your site with JavaScript.

### ← assets

Drag in `assets`, like images or music, to add them to your project

Made by [Glitch](https://glitch.com/)
-------------------

\ ゜o゜)ノ
