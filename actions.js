class Actions {
  play() {
    let reproductor = document.getElementById("audio");

    if (reproductor.paused === true) {
      reproductor.play();
    } else {
      reproductor.pause();
    }
  }

  inicializarActiones() {
    console.log("Constructor");
    let accion = new Actions();
    document.getElementById("botonPlay").addEventListener("click", accion.play);
  }
}

export { Actions };
